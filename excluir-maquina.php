<?php
require 'config.php';
if(empty($_SESSION['cLogin'])) {
	header("Location: login.php");
	exit;
}

require 'classes/maquinas.class.php';
$m = new Maquinas();

if(isset($_GET['id']) && !empty($_GET['id'])) {
	$m->excluirMaquina($_GET['id']);
}

header("Location: maquinas.php");
