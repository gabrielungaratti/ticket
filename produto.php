<?php require 'pages/header.php'; ?>
<?php

require 'classes/anuncios.class.php';
require 'classes/usuarios.class.php';
require 'classes/setor.class.php';

$a = new Anuncios();
$u = new Usuarios();
$s = new Setor();

if(isset($_GET['id']) && !empty($_GET['id'])) {
	$id = addslashes($_GET['id']);
} else {
	?>
	<script type="text/javascript">window.location.href="index.php";</script>
	<?php
	exit;
}

$info = $a->getAnuncio($id);
?>

<div class="container-fluid">
	<div class="row">
		<div class="col-sm-5">

			<div class="carousel slide" data-ride="carousel" id="meuCarousel">
				<div class="carousel-inner" role="listbox">
					<?php foreach($info['fotos'] as $chave => $foto): ?>
					<div class="item <?php echo ($chave=='0')?'active':''; ?>">
						<img src="assets/images/anuncios/<?php echo $foto['url']; ?>" />
					</div>
					<?php endforeach; ?>
				</div>
				<a class="left carousel-control" href="#meuCarousel" role="button" data-slide="prev"><span><</span></a>
				<a class="right carousel-control" href="#meuCarousel" role="button" data-slide="next"><span>></span></a>
			</div>
		</div>
		<div class="col-sm-7">
			<h2>Relatório: </h2>
			<hr>
			<table class="table table-striped">
				<tr>
					<th>Data de abertura: <?php echo date('d/m/Y', strtotime($info['data'])); ?> às <?php echo date('H:i', strtotime($info['data'])); ?></th>
				</tr>
				<tr>
					<th>Estado do atendimento: <?php echo $info['estado']; ?></th>
				</tr>
				<tr>
					<th>Categoria do serviço: <?php echo utf8_encode($info['categoria']); ?></th>
				</tr>
				<tr>
					<th>Código da Máquina :<?php echo $info['valor']; ?></th>
				</tr>
				<tr>
					<th>Setor: <?php echo utf8_encode($info['setor']); ?></th>
				</tr>
				<tr>
					<th>Descrição:<br>
					<?php echo $info['descricao']; ?></th>
				</tr>
			</table>
			<a href="#" class="btn btn-primary" onclick="javascript:window.print();">Gerar relatório</a>
 			<a href="meus-anuncios" class="btn btn-default">Voltar</a>
		</div>
	</div>
</div>


<?php require 'pages/footer.php'; ?>
