<?php require 'pages/header.php';
require 'classes/maquinas.class.php';
$m = new Maquinas();

if(isset($_POST['nome']) && !empty($_POST['nome'])) {
  $nome = addslashes($_POST['nome']);
  $infos = addslashes($_POST['infos']);

	$m->addMaquina($nome, $infos);

	?>
	<div class="alert alert-success">
		Máquina adicionada com sucesso!
	</div>
	<?php
}
?>
<div class="container">
	<h1 >Adicionar máquina à lista</h1>
  <hr>
	<form method="POST">
		<div class="form-group">
			<label for="nome">Código da Máquina:</label>
			<input type="text" name="nome" id="nome" class="form-control" />
		</div>
		<div class="form-group">
			<label for="infos">Descrição técnica da máquina:</label>
			<textarea class="form-control" name="infos"></textarea>
		</div>
		<input type="submit" value="Adicionar" class="btn btn-primary" />
    <a href="maquinas" class="btn btn-default">Voltar</a>
	</form>
</div>
<script type="text/javascript">
document.getElementById("nome").focus();
</script>
<?php require 'pages/footer.php'; ?>
