<?php require 'pages/header.php'; ?>
<?php
if(empty($_SESSION['cLogin'])) {
	?>
	<script type="text/javascript">window.location.href="login.php";</script>
	<?php
	exit;
}

require 'classes/anuncios.class.php';
$a = new Anuncios();

if(isset($_POST['categoria']) && !empty($_POST['categoria'])) {
	$categoria = addslashes($_POST['categoria']);
	$setor = addslashes($_POST['setor']);
	$valor = addslashes($_POST['valor']);
	$descricao = addslashes($_POST['descricao']);
	$estado = addslashes($_POST['estado']);
	if(isset($_FILES['fotos'])) {
		$fotos = $_FILES['fotos'];
	} else {
		$fotos = array();
	}

	$a->editAnuncio($categoria, $setor, $valor, $descricao, $estado, $fotos, $_GET['id']);

	?>
	<div class="alert alert-success">
		Item editado com sucesso!
	</div>
	<?php
}

if(isset($_GET['id']) && !empty($_GET['id'])) {
	$info = $a->getAnuncio($_GET['id']);
} else {
	?>
	<script type="text/javascript">window.location.href="meus-anuncios.php";</script>
	<?php
	exit;
}
?>
<div class="container">
	<h1>Alterar atendimento</h1>

	<form method="POST" enctype="multipart/form-data">
		<div class="form-group">
			<label for="categoria">Categoria:</label>
			<select name="categoria" id="categoria" class="form-control">
				<?php
				require 'classes/categorias.class.php';
				$c = new Categorias();
				$cats = $c->getLista();
				foreach($cats as $cat):
				?>
				<option value="<?php echo $cat['id']; ?>" <?php echo ($info['id_categoria']==$cat['id'])?'selected="selected"':''; ?>><?php echo utf8_encode($cat['nome']); ?></option>
				<?php
				endforeach;
				?>
			</select>
		</div>
		<div class="form-group">
			<label for="categoria">Setor:</label>
			<select name="setor" id="setor" class="form-control">
				<?php
				require 'classes/setor.class.php';
				$s = new Setor();
				$sets = $s->getLista();
				foreach($sets as $set):
				?>
				<option value="<?php echo $set['id']; ?>" <?php echo ($info['id_setor']==$set['id'])?'selected="selected"':''; ?>><?php echo utf8_encode($set['nome']); ?></option>
				<?php
				endforeach;
				?>
			</select>
		</div>
		<div class="form-group">
			<label for="valor">Código da Máquina:</label>
			<input readonly type="text" name="valor" id="valor" class="form-control" value="<?php echo $info['valor']; ?>" />
		</div>
		<div class="form-group">
			<label for="descricao">Descrição:</label>
			<textarea class="form-control" name="descricao"><?php echo $info['descricao']; ?></textarea>
		</div>
		<div class="form-group">
			<label for="estado">Status do atendimento:</label>
			<select name="estado" id="estado" class="form-control">
				<option value="Pendente" <?php echo ($info['estado']=='Pendente')?'selected="selected"':''; ?>>Pendente</option>
				<option value="Em andamento" <?php echo ($info['estado']=='Em andamento')?'selected="selected"':''; ?>>Em andamento</option>
				<option value="Fechado" <?php echo ($info['estado']=='Fechado')?'selected="selected"':''; ?>>Fechado</option>
			</select>
		</div>
		<div class="form-group">
			<label for="add_foto">Fotos do serviço executado:</label>
			<input type="file" name="fotos[]" multiple /><br/>

			<div class="panel panel-default">
				<div class="panel-heading">Album</div>
				<div class="panel-body">
					<?php foreach($info['fotos'] as $foto): ?>
					<div class="foto_item">
						<img src="assets/images/anuncios/<?php echo $foto['url']; ?>" class="img-thumbnail" border="0" /><br/>
						<a href="excluir-foto.php?id=<?php echo $foto['id']; ?>" class="btn btn-default">Excluir Imagem</a>
					</div>
					<?php endforeach; ?>
				</div>
			</div>
		</div>

		<input type="submit" value="Salvar" onClick="javascript:return confirm('Deseja realmente salvar as alterações do registro?');" class="btn btn-default" />
	</form>

</div>
<?php require 'pages/footer.php'; ?>
