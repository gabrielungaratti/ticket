
<?php
 require 'pages/header.php';
if(empty($_SESSION['cLogin'])) {
	?>
	<script type="text/javascript">window.location.href="login.php";</script>
	<?php
	exit;
}
require 'classes/anuncios.class.php';
require 'classes/usuarios.class.php';
require 'classes/categorias.class.php';
require 'classes/setor.class.php';

$a = new Anuncios();
$u = new Usuarios();
$c = new Categorias();
$s = new Setor();

$filtros = array(
	'categoria' => '',
	'setor' => '',
	'estado' => ''
);
if(isset($_GET['filtros'])) {
	$filtros = $_GET['filtros'];
}

$total_anuncios = $a->getTotalAnuncios($filtros);


$p = 1;
if(isset($_GET['p']) && !empty($_GET['p'])) {
	$p = addslashes($_GET['p']);
}

$por_pagina = 10;
$total_paginas = ceil($total_anuncios / $por_pagina);

$anuncios = $a->getUltimosAnuncios($p, $por_pagina, $filtros);
$categorias = $c->getLista();
$setor = $s->getLista();
?>
		<h3 style="text-align:center;"><?php echo 'Seja bem-vindo '.$_SESSION['nome'];?></h2>
<div class="container-fluid">
	<div class="jumbotron">
		<h2>Nós temos hoje <?php echo $total_anuncios; ?> atendimento(s) pendente(s).</h2>
		<br><br>
	<div class="row">
		<div class="col-sm-3">
			<h4>Pesquisa Avançada</h4>
			<form method="GET">
				<div class="form-group">
					<label for="categoria">Categoria:</label>
					<select id="categoria" name="filtros[categoria]" class="form-control">
						<option></option>
						<?php foreach($categorias as $cat): ?>
						<option value="<?php echo $cat['id']; ?>" <?php echo ($cat['id']==$filtros['categoria'])?'selected="selected"':''; ?>><?php echo utf8_encode($cat['nome']); ?></option>
						<?php endforeach; ?>
					</select>
				</div>
				<div class="form-group">
					<label for="setor">Setor:</label>
					<select id="setor" name="filtros[setor]" class="form-control">
						<option></option>
						<?php foreach($setor as $set): ?>
						<option value="<?php echo $set['id']; ?>" <?php echo ($set['id']==$filtros['setor'])?'selected="selected"':''; ?>><?php echo utf8_encode($set['nome']); ?></option>
						<?php endforeach; ?>
					</select>
				</div>
				<div class="form-group">
					<label for="estado">Estado do atendimento:</label>
					<select id="estado" name="filtros[estado]" class="form-control">
						<option></option>
						<option value="Pendente" <?php echo ($filtros['estado']=='Pendente')?'selected="selected"':''; ?>>Pendente</option>
						<option value="Executando" <?php echo ($filtros['estado']=='Executando')?'selected="selected"':''; ?>>Em andamento</option>
						<option value="Fechado" <?php echo ($filtros['estado']=='Fechado')?'selected="selected"':''; ?>>Fechado</option>
					</select>
				</div>

				<div class="form-group">
					<input type="submit" class="btn btn-info" value="Buscar" />
				</div>
			</form>

		</div>
		<div class="col-sm-9">
			<table class="table table-striped">
				<thead class="thead-light">
				<tr>
					<th>#</th>
					<th>Código da Máquina</th>
          <th>Data de abertura</th>
					<th>Tipo</th>
					<th>Setor</th>
					<th>Status</th>
					<th>Ações</th>
				</tr>
				</thead>
				<tbody>

					<?php foreach($anuncios as $anuncio): ?>
					<tr>
						<td>
							<?php if(!empty($anuncio['url'])): ?>
							<img src="assets/images/anuncios/<?php echo $anuncio['url']; ?>" height="50" border="0" />
							<?php else: ?>
							<img src="assets/images/default.jpg" height="50" border="0" />
							<?php endif; ?>
						</td>
						<td>
							<?php echo utf8_encode($anuncio['valor']); ?>
						</td>
            <td>
                <?php echo date('d/m/Y', strtotime($anuncio['data'])); ?>
            </td>
						<td>
							<a href="produto.php?id=<?php echo $anuncio['id']; ?>"><?php echo utf8_encode($anuncio['categoria']); ?></a><br/>
						</td>
						<td>
							<?php echo utf8_encode($anuncio['setor']); ?>
						</td>
						<td>
							<?php echo utf8_encode($anuncio['estado']); ?>
						</td>
						<td>
							<a href="editar-anuncio.php?id=<?php echo $anuncio['id']; ?>" class="btn btn-default">Editar</a>
							<a href="excluir-anuncio.php?id=<?php echo $anuncio['id']; ?>" onClick="javascript:return confirm('Deseja realmente excluir esse registro? É uma ação irreversível!');" class="btn btn-danger">Excluir</a>
						</td>
					</tr>
					<?php endforeach; ?>
				</tbody>
			</table>

			<ul class="pagination">
				<?php for($q=1;$q<=$total_paginas;$q++): ?>
				<li class="<?php echo ($p==$q)?'active':''; ?>"><a href="meus-anuncios.php?p=<?php echo $q; ?>"><?php echo $q; ?></a></li>
				<?php endfor; ?>
			</ul>
		</div>
	</div>


</div>
</div>

<?php require 'pages/footer.php'; ?>
