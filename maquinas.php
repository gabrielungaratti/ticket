<?php require 'pages/header.php';
if(empty($_SESSION['cLogin'])) {
	?>
	<script type="text/javascript">window.location.href="login.php";</script>
	<?php
	exit;
}
require 'classes/maquinas.class.php';
$m = new Maquinas();
$s = '';

if(!empty($_GET['busca'])) {
	$s = $_GET['busca'];
}

$maquinas = $m->getLista($s);

?>

<div class="container">
<a href="adicionar-maquina" class="btn btn-primary">Cadastrar</a>
<a href="meus-anuncios" class="btn btn-default">Voltar</a><br/><br/>
<fieldset>
	<form method="GET">
		<input  type="text" id="busca" name="busca" value="<?php echo (!empty($_GET['busca']))?$_GET['busca']:''; ?>" placeholder="Digite o código da máquina" style="width:100%;height:40px;font-size:18px;" />
	</form>
</fieldset>
<hr>
	<table class="table  table-striped table-sm " width="100%">
		<thead class="thead-light">
		<tr>
			<th>Cód. da máquina</th>
			<th>Descrição</th>
			<th>Ações</th>
		</tr>
		</thead>
		<tbody>
		<?php foreach($maquinas as $maquina): ?>
			<tr>
				<td><?php echo $maquina['nome']; ?></td>
				<td><?php echo $maquina['infos']; ?></td>
				<td>
						<a href="editar-maquinas.php?id=<?php echo $maquina['id']; ?>" class="btn btn-default">Editar</a>
						<a href="excluir-maquina.php?id=<?php echo $maquina['id']; ?>" onClick="javascript:return confirm('Deseja realmente excluir esse registro? É uma ação irreversível!');" class="btn btn-danger">Excluir</a>
				</td>
			</tr>
		<?php endforeach; ?>
		</div>
	</tbody>
	</table>
</div>

<script type="text/javascript">
document.getElementById("busca").focus();
</script>
<?php require 'pages/footer.php'; ?>
