<?php require 'pages/header.php'; ?>
<?php
if(empty($_SESSION['cLogin'])) {
	?>
	<script type="text/javascript">window.location.href="login.php";</script>
	<?php
	exit;
}

require 'classes/maquinas.class.php';
$m = new Maquinas();

if(isset($_POST['nome']) && !empty($_POST['nome'])) {
	$nome = addslashes($_POST['nome']);
	$infos = addslashes($_POST['infos']);

	$m->editMaquina($nome, $infos, $_GET['id']);

	?>
	<div class="alert alert-success">
		Item editado com sucesso!
	</div>
	<?php
}

if(isset($_GET['id']) && !empty($_GET['id'])) {
	$maquinas = $m->getMaquinas($_GET['id']);
} else {
	?>
	<script type="text/javascript">window.location.href="maquinas.php";</script>
	<?php
	exit;
}
?>
<div class="container">
	<h1>Editar informações das máquinas</h1>
	<?php  ?>
	<form method="POST" enctype="multipart/form-data">
		<div class="form-group">
			<label for="nome">Código da Máquina:</label>
			<input readonly type="text" name="nome" id="nome" class="form-control" value="<?php echo $maquinas['nome']; ?>" />
		</div>
		<div class="form-group">
			<label for="infos">Descrição:</label>
			<textarea class="form-control" name="infos"><?php echo $maquinas['infos']; ?></textarea>
		</div>
		<input type="submit" value="Salvar" onClick="javascript:return confirm('Deseja realmente salvar as alterações do registro?');" class="btn btn-primary" />
		<a href="maquinas" class="btn btn-default">Voltar</a>
	</form>

</div>
<?php require 'pages/footer.php'; ?>
