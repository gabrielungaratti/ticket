<?php require 'config.php';
date_default_timezone_set('America/Sao_Paulo'); ?>
<html>
<head>
	<title>Help Desk</title>
	<link rel="stylesheet" href="assets/css/bootstrap.min.css" />
	<link rel="stylesheet" href="assets/css/style.css" />
	<script type="text/javascript" src="assets/js/jquery.min.js"></script>
	<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="assets/js/script.js"></script>
</head>
<body>
	<nav class="navbar navbar-inverse">
		<div class="container-fluid">
			<div class="navbar-header">
				<a href="./" class="navbar-brand">Help Desk</a>
			</div>
			<ul class="nav navbar-nav navbar-right">
				<?php if(isset($_SESSION['cLogin']) && !empty($_SESSION['cLogin'])): ?>
					<li><a href="maquinas">Máquinas cadastradas</a></li>
					<li><a href="meus-anuncios">Tickets abertos</a></li>
					<li><a href="sair">Sair</a></li>
				<?php else: ?>
					<!--<li><a href="cadastre-se.php">Cadastre-se</a></li>-->
					<li><a href="login.php">Login</a></li>
				<?php endif; ?>
			</ul>
		</div>
	</nav>
