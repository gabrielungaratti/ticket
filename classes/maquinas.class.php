<?php
class Maquinas {

  public function addMaquina($nome, $infos) {
    global $pdo;


    $sql = $pdo->prepare("INSERT INTO maquinas SET nome = :nome, infos = :infos");
    $sql->bindValue(":nome", $nome);
    $sql->bindValue(":infos", $infos);
    $sql->execute();
  }

  public function editMaquina($nome, $infos, $id) {
		global $pdo;

		$sql = $pdo->prepare("UPDATE maquinas SET nome = :nome, infos = :infos WHERE id = :id");
		$sql->bindValue(":nome", $nome);
		$sql->bindValue(":infos", $infos);
		$sql->bindValue(":id", $id);
		$sql->execute();

}

public function excluirMaquina($id) {
  global $pdo;

  $sql = $pdo->prepare("DELETE FROM maquinas WHERE id = :id");
  $sql->bindValue(":id", $id);
  $sql->execute();
  }

  public function getMaquinas($s='') {
		global $pdo;
		$array = array();

		$sql = $pdo->prepare("SELECT * FROM maquinas");
		$sql->execute();

		if($sql->rowCount() > 0) {
			$array = $sql->fetch();
		}

		return $array;
	}

  public function getLista($s='') {
		global $pdo;
		$array = array();

    if(!empty($s)) {
      $sql = $pdo->prepare("SELECT * FROM maquinas WHERE nome = :nome or infos LIKE :infos");
      $sql->bindValue(":nome", $s);
      $sql->bindValue(":infos", '%'.$s.'%');
      $sql->execute();
    }else {
      $sql = $pdo->prepare("SELECT * FROM maquinas");
      $sql->execute();
    }



		if($sql->rowCount() > 0) {
			$array = $sql->fetchAll();
		}

		return $array;
	}


}
?>
