<?php require 'pages/header.php';
require 'classes/anuncios.class.php';
$a = new Anuncios();

if(isset($_POST['categoria']) && !empty($_POST['categoria'])) {
  $categoria = addslashes($_POST['categoria']);
  $setor = addslashes($_POST['setor']);
	$valor = addslashes($_POST['valor']);
	$descricao = addslashes($_POST['descricao']);
  date_default_timezone_set('America/Sao_Paulo');
  $data = date("Y-m-d H:i:s");
	$estado = "Pendente";
	$a->addAnuncio($categoria, $setor, $valor, $descricao, $data, $estado);

	?>
	<div class="alert alert-success">
		Pedido adicionado com sucesso!
	</div>
	<?php
}
?>
<div class="container">
	<h1 >Solicitar Ticket</h1>
  <hr>
	<form method="POST">
		<div class="form-group">
			<label for="categoria">Problema:</label>
			<select name="categoria" id="categoria" class="form-control">
				<?php
				require 'classes/categorias.class.php';
				$c = new Categorias();
				$cats = $c->getLista();
				foreach($cats as $cat):
				?>
				<option value="<?php echo $cat['id']; ?>"><?php echo utf8_encode($cat['nome']); ?></option>
				<?php
				endforeach;
				?>
			</select>
		</div>
    <div class="form-group">
      <label for="setor">Setor da máquina:</label>
      <select name="setor" id="setor" class="form-control">
        <?php
        require 'classes/setor.class.php';
        $s = new Setor();
        $sets = $s->getLista();
        foreach($sets as $set):
        ?>
        <option value="<?php echo $set['id']; ?>"><?php echo utf8_encode($set['nome']); ?></option>
        <?php
        endforeach;
        ?>
      </select>
    </div>
		<div class="form-group">
			<label for="valor">Código da Máquina:</label>
			<input type="text" name="valor" id="valor" class="form-control" />
		</div>
		<div class="form-group">
			<label for="descricao">Descrição do problema:</label>
			<textarea class="form-control" name="descricao"></textarea>
		</div>
		<input type="submit" value="Adicionar" class="btn btn-default" />
	</form>
</div>
<script type="text/javascript">
document.getElementById("categoria").focus();
</script>
<?php require 'pages/footer.php'; ?>
